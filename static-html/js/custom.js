$(function () {
$('.calender-item').hide();

      $('#group-fitness-vc .select-gym select').change(function(){
    
    $('#' + $(this).val()).slideDown(300);
  });


$('.class-schedule').parents('td').css({"position":"relative", "padding":"0"});
   

    /**
     * table canlender (show and hide clasess)
     */
    var $frClass= $("#group-fitness-vc  .class-schedule.fr-class");
    var $engClass=$("#group-fitness-vc  .class-schedule.eng-class");

    $('.select-choice input#fr').on('click', function(){
   
    if ( $(this).is(':checked') ){
         $frClass.show();
         $frClass.css({"background-color": "#ffc130"});
         }
    else{
         $frClass.hide();
        $frClass.css({"background-color": "transparent"});
     }
});

   $('.select-choice input#en').on('click', function(){
   
    if ( $(this).is(':checked') ){
         $engClass.show();
         $engClass.css({"background-color": "#ffc130"});
         }
    else{
         $engClass.hide();
        $engClass.css({"background-color": "transparent"});
     }
});


    /**
     * View all classes 
     */ 
 

    window.onresize = window.onload = function() {
  if ($(window).width() < 700) {
    $("#group-fitness .title-categorie").each(function() {

    $(this).find(".workout-tip").hide();
   $(this).find(".workout-tip").slice(0, 1).show();
    });
  }
 else {
       $("#group-fitness .title-categorie").each(function() {

    $(this).find(".workout-tip").slice(0, 3).show();
    });
  }
    }




      $(".more-classes a").on('click', function (e) {
        e.preventDefault();
        $(this).parents('.title-categorie').find(".workout-tip:hidden").slice(0, 3).slideDown();
        if ($(this).parents('.title-categorie').find(".workout-tip:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        if ($(this).parents('.title-categorie').find(".workout-tip:hidden").length == 0) {
            $(this).parents('.title-categorie').find(".more-classes a").fadeOut('slow');
        }

    });



    /**
     * 
     */ 
$('#group-fitness .four-categories .item a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - 70
                }, 1000);
                return false
            }
        }
    });

    $("#membership-page .panel-default, .platinum-zone-tanning .panel-default").slice(0, 3).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $(".panel-default:hidden").slice(0, 3).slideDown();
        if ($(".panel-default:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        if ($(".panel-default:hidden").length == 0) {
            $("#loadMore").fadeOut('slow');
        }

    });


    $(".video-link").jqueryVideoLightning({
autoplay: 1,
backdrop_color: "#ddd",
backdrop_opacity: 0.6,
glow: 20,
glow_color: "#000"
});


});



